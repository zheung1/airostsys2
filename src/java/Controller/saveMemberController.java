/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Member;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "saveMemberController", urlPatterns = {"/saveMemberController"})
public class saveMemberController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        //try (PrintWriter out = response.getWriter()) {
         
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
        String name = request.getParameter("name");
        String course = request.getParameter("course"); 
        String faculty = request.getParameter("faculty");
        String phone = request.getParameter("phone");
        String email = request.getParameter("email"); 
        String position = request.getParameter("position");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        
        String[] skillName = {"","","","","","","","","",""};
        String[] skillLevel = {"","","","","","","","","",""};
        
        String[] achievement = {"","","","","","","","","",""};
        String[] achievementYear = {"","","","","","","","","",""};
        
        for(int i=0; i<10; i++){
            String s = request.getParameter("skill"+i);
            String level = request.getParameter("skillLevel"+i);
            
            if(s!=null){
                skillName[i] = s;
                skillLevel[i] = level;
            }else{
                continue;
            }
        }
        
        for(int i=0; i<10; i++){
            String a = request.getParameter("achievement"+i);
            String year = request.getParameter("achievementYear"+i);
            
            if(a!=null){
                achievement[i] = a;
                achievementYear[i] = year;
            }else{
                continue;
            }
        }
        
        Member member = new Member();
        member.setName(name);
        member.setCourse(course);
        member.setFaculty(faculty);
        member.setPhone(phone);
        member.setEmail(email);
        member.setPosition(position);
        member.setUsername(username);
        member.setPassword(password);
        member.setSkillName(skillName);
        member.setSkillLevel(skillLevel);
        member.setAchievement(achievement);
        member.setAchievementYear(achievementYear);
        
        PrintWriter writer = response.getWriter();
            
        try{
            JDBCUtility j = new JDBCUtility();
            j.jdbcConnect();
            j.prepareStatement();
            PreparedStatement ps = j.psInsertMember();
            
            ps.setString(1, member.getName());
            ps.setString(2, member.getCourse());
            ps.setString(3, member.getFaculty());
            ps.setString(4, member.getPhone());
            ps.setString(5, member.getEmail());
            ps.setString(6, Arrays.toString(member.getAchievement()));
            ps.setString(7, Arrays.toString(member.getAchievementYear()));
            ps.setString(8, Arrays.toString(member.getSkillName()));
            ps.setString(9, Arrays.toString(member.getSkillLevel()));
            ps.setString(10, member.getPosition());
            ps.setString(11, member.getUsername());
            ps.setString(12, member.getPassword());
            
            ps.execute();
        }catch(SQLException e){
            writer.print(e.getMessage());
        }catch(Exception e){
            writer.print(e.getMessage());
        }
        
        response.sendRedirect("teamController");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
