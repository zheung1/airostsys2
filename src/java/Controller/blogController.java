/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Item;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;


@WebServlet(name = "blogController", urlPatterns = {"/blogController"})
public class blogController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
                 processRequest(request, response);
        PrintWriter writer = response.getWriter();
        
            String s = ""; 
            String title="";
            String author = null;
            String date = null;
            String description = null;
            String filenama="";
            String imagename = null;
            
        if(ServletFileUpload.isMultipartContent(request)){
            try {
                List<FileItem> multiparts = new ServletFileUpload(
                                         new DiskFileItemFactory()).parseRequest(request);
               
                for(FileItem item : multiparts){
                    if(!item.isFormField()){
                        imagename = UUID.randomUUID().toString()+".png";
                        
                        item.write( new File(getServletContext().getRealPath("") + File.separator + "images" +File.separator+ imagename));
                    }else{
                        if(item.getFieldName().equals("title")){
                            title = item.getString();
                        }
                        else if(item.getFieldName().equals("author")){
                            author = item.getString();
                        }
                        else if(item.getFieldName().equals("date")){
                            date = item.getString();
                        }
                        else if(item.getFieldName().equals("description")){
                            description = item.getString();
                        }
                        else if(item.getFieldName().equals("filename")){
                            filenama = item.getString();
                        }
                        
                        
                    }
                }
                //File uploaded successfully
               writer.println("File Uploaded Successfully" + getServletContext().getRealPath("") + File.separator + "images" +File.separator+ filenama);
        
           
        try {
            String UPLOAD_DIRECTORY = "blogs",
                    filename = title + ".html";
            
            
                File f = new File(getServletContext().getRealPath("") + File.separator + UPLOAD_DIRECTORY +File.separator+ filename);
                FileWriter fstream = new FileWriter(f, true);
                BufferedWriter out = new BufferedWriter(fstream);
                out.write(s);
                out.close();
                fstream.close();
            
            JDBCUtility j = new JDBCUtility();
            j.jdbcConnect();
            j.prepareStatement();
            PreparedStatement ps = j.psPostBlog();
            ps.setString(1, title);
            ps.setString(2, author);
            ps.setString(3, date);
            ps.setString(4, description);
            ps.setString(5, filename);
            ps.setString(6, imagename);
            
            ps.execute();
            
            
        }catch(SQLException ex){
            writer.print(ex);
        }
            
        } catch (SQLException ex) {
            writer.print(ex.getMessage());
        }            catch (FileUploadException ex) {
                         writer.print(ex.getMessage());
                     } catch (Exception ex) {
                         writer.print("x" +ex.getMessage());
                     }
        
    }
        
    //response.sendRedirect("blogEditor.jsp");
        /*PrintWriter write = response.getWriter();
        try{
            processRequest(request, response);
            
            String s = request.getParameter("blogContent");
            String title = request.getParameter("title");
            String author = request.getParameter("author");
            String date = request.getParameter("date");
            String description = request.getParameter("description");
            
            String UPLOAD_DIRECTORY = "blogs",
                    filename = title + ".html";
            
            try{
                File f = new File(getServletContext().getRealPath("") + File.separator + UPLOAD_DIRECTORY +File.separator+ filename);
                FileWriter fstream = new FileWriter(f, true);
                BufferedWriter out = new BufferedWriter(fstream);
                out.write(s);
                out.close();
                fstream.close();
                /*
                FileReader r = new FileReader(getServletContext().getRealPath("") + File.separator + UPLOAD_DIRECTORY +File.separator+ filename);
                BufferedReader reader = new BufferedReader(r);
                String text = "";
                String read="";
                do{
                read = reader.readLine();
                text += read;
                
                }while(!read.equals(""));
                r.close(); reader.close();
                out.write(text);
                
            }catch(IOException e){
                write.print(e);
            }
            
            JDBCUtility j = new JDBCUtility();
            j.jdbcConnect();
            j.prepareStatement();
            PreparedStatement ps = j.psPostBlog();
            ps.setString(1, title);
            ps.setString(2, author);
            ps.setString(3, date);
            ps.setString(4, description);
            ps.setString(5, filename);
            
            ps.execute();
            
            
        }catch(SQLException ex){
            write.print(ex);
        }
        
        response.sendRedirect("blogEditor.jsp");*/
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
