/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Item;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "saveProjectChangesController", urlPatterns = {"/saveProjectChangesController"})
public class saveProjectChangesController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        //try (PrintWriter out = response.getWriter()) {
        int id = Integer.parseInt(request.getParameter("id"));
        String projectName = request.getParameter("name"),
        projectClient = request.getParameter("client");  
        String[] phases = {"","","","","","","","","",""};
        int[] weightages = {0,0,0,0,0,0,0,0,0,0};
        String[] sDates = {"","","","","","","","","",""};
        String[] eDates = {"","","","","","","","","",""};
        String[] statuses = {"","","","","","","","","",""};
        
        for(int i=0; i<10; i++){
            String phase = request.getParameter("phaseName"+i);
            int weightage = Integer.parseInt(request.getParameter("phaseWeightage"+i));
            String sDate = request.getParameter("phaseSdate"+i);
            String eDate = request.getParameter("phaseEdate"+i);
            String status = request.getParameter("phaseStatus"+i);
            
            if(!phase.equals("")){
                phases[i] = phase;
            }
            if(weightage>=0){
                weightages[i] = weightage;
            }
            if(!sDate.equals("")){
                sDates[i] = sDate;
            }
            if(!eDate.equals("")){
                eDates[i] = eDate;
            }
            
            //writer.print(status+"<br>");
                statuses[i] = status;
            
        }
        
        
        JDBCUtility j = new JDBCUtility();
          j.jdbcConnect();
          j.prepareStatement();
          PreparedStatement ps = j.psEditProject();
          //name=?, client=?, description=?, percentage=?, startDate=?, endDate=?, status=? WHERE id=?";
          ps.setString(1, projectName);
          ps.setString(2, projectClient);
          ps.setString(3, Arrays.toString(phases));
          ps.setString(4, Arrays.toString(weightages));
          ps.setString(5, Arrays.toString(sDates));
          ps.setString(6, Arrays.toString(eDates));
          ps.setString(7, Arrays.toString(statuses));
          ps.setInt(8, id);
          
         ps.executeUpdate();
          
          RequestDispatcher r = request.getRequestDispatcher("projectController");
          r.forward(request, response);   
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(saveProjectChangesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(saveProjectChangesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
