/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Project;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "projectController", urlPatterns = {"/projectController"})
public class projectController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        
        //RequestDispatcher rd = request.getRequestDispatcher("projectManager.jsp");
        //rd.forward(request,response);
         PrintWriter writer = response.getWriter();
        try{
        JDBCUtility j = new JDBCUtility();
        j.jdbcConnect();
        j.prepareStatement();
        
        PreparedStatement st = j.psDisplayProject();
        ResultSet rs = st.executeQuery();
        
        ArrayList<Project> projects = new ArrayList<Project>();
        
        int id=0;
        String name = "", client= "", phase="", percentage= "", startDate= "", endDate= "", status="";
        String[] phases, percentages, startDates, endDates, statuses = new String[10];
       
        while(rs.next()){
            id = rs.getInt("id");
            name = rs.getString("name");
            client = rs.getString("client");
            phase = rs.getString("description");
            percentage = rs.getString("percentage");
            startDate = rs.getString("startDate");
            endDate = rs.getString("endDate");
            status = rs.getString("status");
            
            phase = phase.substring(1, phase.length()-1);
            percentage = percentage.substring(1, percentage.length()-1);
            startDate = startDate.substring(1, startDate.length()-1);
            endDate = endDate.substring(1, endDate.length()-1);
            status = status.substring(1, status.length()-1);
            
            phases = phase.split(",");
            percentages = percentage.split(",");
            startDates = startDate.split(",");
            endDates = endDate.split(",");
            statuses = status.split(",");
            
            Project project = new Project();
            project.setId(id);
            project.setName(name);
            project.setClient(client);
            project.setDescription(phases);
            project.setPercentage(percentages);
            project.setStartDate(startDates);
            project.setEndDate(endDates);
            project.setStatus(statuses);
            
            int completed = 0;
            for(int i=0; i<10; i++){
                if(statuses[i].replaceAll("\\s","").equals("completed"))
                    completed += Double.parseDouble(percentages[i]);
                else
                    continue;
            }
            project.setPercentCompleted(completed);
            if(completed  == 100){
                project.setProgress("completed");
            }else{
                project.setProgress("ongoing");
            }
            
            projects.add(project);
        }
        HttpSession s = request.getSession();
        s.setAttribute("projectList", projects);
        RequestDispatcher r = request.getRequestDispatcher("projectManager.jsp");
        r.forward(request, response);
        }catch(SQLException e){
            writer.write("s:"+e.getMessage());
        }catch(Exception e){
            writer.write("e:"+e.getMessage());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(projectController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(projectController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
