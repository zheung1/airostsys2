/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Item;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;


@WebServlet(name = "saveItemController", urlPatterns = {"/saveItemController"})
public class saveItemController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private final String UPLOAD_DIRECTORY = "images";
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         processRequest(request, response);
        PrintWriter writer = response.getWriter();
        
        String name ="",category="",description="",filename="",location="", amount="";
        if(ServletFileUpload.isMultipartContent(request)){
            try {
                List<FileItem> multiparts = new ServletFileUpload(
                                         new DiskFileItemFactory()).parseRequest(request);
               
                for(FileItem item : multiparts){
                    if(!item.isFormField()){
                        filename = UUID.randomUUID().toString()+".png";
                        
                        item.write( new File(getServletContext().getRealPath("") + File.separator + UPLOAD_DIRECTORY +File.separator+ filename));
                    }else{
                        if(item.getFieldName().equals("name")){
                            name = item.getString();
                        }
                        else if(item.getFieldName().equals("category")){
                            category = item.getString();
                        }
                        else if(item.getFieldName().equals("description")){
                            description = item.getString();
                        }
                        else if(item.getFieldName().equals("location")){
                            location = item.getString();
                        }
                        else if(item.getFieldName().equals("amount")){
                            amount = item.getString();
                        }
                        
                        
                    }
                }
                //File uploaded successfully
               writer.println("File Uploaded Successfully" + getServletContext().getRealPath("") + File.separator + UPLOAD_DIRECTORY +File.separator+ filename);
        JDBCUtility j = new JDBCUtility();
        j.jdbcConnect();
        //RequestDispatcher rd = request.getRequestDispatcher("./UploadItem.jsp");
           
        try {
            j.prepareStatement();
            writer.print("<script>"
                    + "window.onload = function(){"
                    + "document.getElementById('msg').style.visibility='visible';"
                    + "}"
                    + "</script>");
            
        } catch (SQLException ex) {
            writer.print(ex.getMessage());
        }
        Item item = new Item();
        item.setName(name);
        item.setCategory(category);
        item.setLocation(location);
        item.setAmount(Integer.parseInt(amount));
        item.setDescription(description);
        item.setImage(filename);
        
        PreparedStatement st = j.psInsertItem();
        st.setString(1, item.getName());
        st.setString(2, item.getCategory());
        st.setString(3, item.getLocation());
        st.setInt(4, item.getAmount());
        st.setString(5, item.getDescription());
        st.setString(6, item.getImage());
        
        st.execute();
       
    }catch(SQLException ex){
        writer.print(ex.getMessage());
    }        catch (Exception ex) {
                 //Logger.getLogger(saveItemController.class.getName()).log(Level.SEVERE, null, ex);
                 writer.print(ex.getMessage());
             }}
        
    response.sendRedirect("inventoryController");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
