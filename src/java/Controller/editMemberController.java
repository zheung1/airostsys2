/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Member;
import Model.Project;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "editMemberController", urlPatterns = {"/editMemberController"})
public class editMemberController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        
        try{
        int id = Integer.parseInt(request.getParameter("id"));
        JDBCUtility j = new JDBCUtility();
        j.jdbcConnect();
        j.prepareStatement();
        PreparedStatement ps = j.psShowMember();
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        
        String name, course, faculty, tel, email, position;
        String[] achievements, achievementYear, skill, skillLevel = new String[10];
        String achievementList, achievementYears, skills, skillLevels;
        
        Member member = new Member();
        
        while(rs.next()){
            id = rs.getInt("id");
            name = rs.getString("name");
            course = rs.getString("course");
            faculty = rs.getString("faculty");
            tel = rs.getString("tel");
            email = rs.getString("email");
            position = rs.getString("position");
            
            achievementList = rs.getString("achivements");
            achievementYears =rs.getString("achievementYear");
            skills =rs.getString("skills");
            skillLevels =rs.getString("skillLevel");
            
            achievementList = achievementList.substring(0, achievementList.length()-1);
            achievementYears = achievementYears.substring(1, achievementYears.length()-1).replaceAll("//s", "");
            skills = skills.substring(0, skills.length()-1);
            skillLevels = skillLevels.substring(1, skillLevels.length()-1).replaceAll("//s", "");
            
            achievements = achievementList.split(",");
            achievementYear = achievementYears.split(",");
            skill = skills.split(",");
            skillLevel = skillLevels.split(",");
            
            member.setId(id);
            member.setName(name);
            member.setCourse(course);
            member.setFaculty(faculty);
            member.setPhone(tel);
            member.setEmail(email);
            member.setPosition(position);
            member.setAchievement(achievements);
            member.setAchievementYear(achievementYear);
            member.setSkillName(skill);
            member.setSkillLevel(skillLevel);
            
            HttpSession s = request.getSession();
            s.setAttribute("member", member);
            RequestDispatcher rd = request.getRequestDispatcher("editMember.jsp");
            rd.forward(request, response);
        }
        
        }catch(SQLException e){
            writer.print(e);
        }catch(Exception e){
            writer.print(e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(editMemberController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(editMemberController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
