/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class JDBCUtility{
    private String driver = "com.mysql.jdbc.Driver";
    private String url = "jdbc:mysql://localhost/airostdb?useTimezone=true&serverTimezone=UTC&autoReconnect=true&useSSL=false";
    private String username = "root";
    private String password = "alpine208";
    private String insertItemQuery = "INSERT INTO inventory (name, category, location, amount, description,image) VALUES (?,?,?,?,?,?)";
    private String displayItemQuery = "SELECT * FROM inventory ORDER BY name ASC";
    private String removeItemQuery = "DELETE FROM inventory WHERE id=?";
    private String editItemQuery = "UPDATE inventory SET name=?, category=?, location=?, amount=?, description=? WHERE id=?";
    private String showItemQuery = "SELECT * FROM inventory WHERE id=?";
    private String displayProjectQuery = "SELECT * FROM project ORDER BY name ASC";
    private String insertProjectQuery = "INSERT INTO project (name, client, description, percentage, startDate, endDate, status) VALUES (?,?,?,?,?,?,?)";
    private String showProjectQuery = "SELECT * FROM project WHERE id=?";
    private String editProjectQuery = "UPDATE project SET name=?, client=?, description=?, percentage=?, startDate=?, endDate=?, status=? WHERE id=?";
    private String removeProjectQuery = "DELETE FROM project WHERE id=?";
    private String insertMemberQuery = "INSERT INTO members (name, course, faculty, tel, email, achivements, achievementYear, skills,skillLevel, position, username, password) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
    private String displayMembersQuery = "SELECT * FROM members ORDER BY name ASC";
    private String showMemberQuery = "SELECT * FROM members WHERE id=?";
    private String editMemberQuery = "UPDATE members SET name=?, course=?, faculty=?, tel=?, email=?, achivements=?, achievementYear=?, skills=?,skillLevel=?, position=? where id=?";
    private String removeMemberQuery = "DELETE FROM members WHERE id=?";
    private String loginQuery = "SELECT * FROM members WHERE username=? AND password=?";
    private String postBlogQuery = "INSERT INTO content (title, author, date, description, filename, image) values (?,?,?,?,?,?)";
    private String displayBlogQuery = "SELECT * FROM content";
    private String storeAttendanceQuery = "INSERT INTO attendance (name, date, time) values (?,?,?)";
    private String viewAttendanceQuery = "SELECT * FROM attendance";
    
    private Connection con;
    
    private PreparedStatement psInsertItem = null;
    private PreparedStatement psDisplayItem = null;
    private PreparedStatement psRemoveItem = null;
    private PreparedStatement psEditItem = null;
    private PreparedStatement psShowItem = null;
    private PreparedStatement psDisplayProject = null;
    private PreparedStatement psInsertProject = null;
    private PreparedStatement psShowProject = null;
    private PreparedStatement psEditProject = null;
    private PreparedStatement psInsertMember = null;
    private PreparedStatement psDisplayMembers = null;
    private PreparedStatement psShowMember = null;
    private PreparedStatement psEditMember = null;
    private PreparedStatement psRemoveProject = null;
    private PreparedStatement psRemoveMember = null;
    private PreparedStatement psLogin = null;
    private PreparedStatement psPostBlog = null;
    private PreparedStatement psDisplayBlog = null;
    private PreparedStatement psStoreAttendanceQuery = null;
    private PreparedStatement psViewAttendanceQuery = null;
    
    
    public void jdbcConnect(){
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url, username, password);
  
        } catch (SQLException ex) {
            System.out.print("exception : "+ex.toString());
           
        } catch (ClassNotFoundException ex) {
            //Logger.getLogger(UploadItem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void prepareStatement() throws SQLException{
        con = DriverManager.getConnection(url, username, password);
        psInsertItem = con.prepareStatement(insertItemQuery);
        psDisplayItem = con.prepareStatement(displayItemQuery);
        psRemoveItem = con.prepareStatement(removeItemQuery);
        psEditItem = con.prepareStatement(editItemQuery);
        psShowItem = con.prepareStatement(showItemQuery);
        psDisplayProject = con.prepareStatement(displayProjectQuery);
        psInsertProject = con.prepareStatement(insertProjectQuery);
        psShowProject = con.prepareStatement(showProjectQuery);
        psEditProject = con.prepareStatement(editProjectQuery);
        psInsertMember = con.prepareStatement(insertMemberQuery);
        psDisplayMembers = con.prepareStatement(displayMembersQuery);
        psShowMember = con.prepareStatement(showMemberQuery);
        psEditMember = con.prepareStatement(editMemberQuery);
        psRemoveProject = con.prepareStatement(removeProjectQuery);
        psRemoveMember = con.prepareStatement(removeMemberQuery);
        psLogin = con.prepareStatement(loginQuery);
        psPostBlog = con.prepareStatement(postBlogQuery);
        psDisplayBlog = con.prepareStatement(displayBlogQuery);
        psStoreAttendanceQuery = con.prepareStatement(storeAttendanceQuery);
        psViewAttendanceQuery = con.prepareStatement(viewAttendanceQuery);
    }
    
    public PreparedStatement psInsertItem(){
        return psInsertItem;
    }
    
    public PreparedStatement psDisplayItem(){
        return psDisplayItem;
    }
    public PreparedStatement psRemoveItem(){
        return psRemoveItem;
    }
    public PreparedStatement psEditItem(){
        return psEditItem;
    }
    public PreparedStatement psShowItem(){
        return psShowItem;
    }
    public PreparedStatement psDisplayProject(){
        return psDisplayProject;
    }
    public PreparedStatement psInsertProject(){
        return psInsertProject;
    }
    public PreparedStatement psShowProject(){
        return psShowProject;
    }
    public PreparedStatement psEditProject(){
        return psEditProject;
    }
     public PreparedStatement psInsertMember(){
        return psInsertMember;
    }
     public PreparedStatement psDisplayMembers(){
        return psDisplayMembers;
    }
      public PreparedStatement psShowMember(){
        return psShowMember;
    }
       public PreparedStatement psEditMember(){
        return psEditMember;
    }
       public PreparedStatement psRemoveProject(){
        return psRemoveProject;
    }
       public PreparedStatement psRemoveMember(){
        return psRemoveMember;
    }
        public PreparedStatement psLogin(){
        return psLogin;
    }
      public PreparedStatement psPostBlog(){
        return psPostBlog;
    }
    public PreparedStatement psDisplayBlog(){
        return psDisplayBlog;
    }
    public PreparedStatement psStoreAttendanceQuery(){
        return psStoreAttendanceQuery;
    }
    public PreparedStatement psViewAttendanceQuery(){
        return psViewAttendanceQuery;
    }
    
    
    
}

