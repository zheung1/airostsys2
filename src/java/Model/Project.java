/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

public class Project {
    private String name, client;
    private String[] description = {"","","","","","","","","",""}, 
            startDate= {"","","","","","","","","",""}, 
            endDate= {"","","","","","","","","",""};
    private String[] status= {"","","","","","","","","",""};
    private String[] percentage= {"","","","","","","","","",""};
    private int id;
    private int percentCompleted;
    private String progress;

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public int getPercentCompleted() {
        return percentCompleted;
    }

    public void setPercentCompleted(int percentCompleted) {
        this.percentCompleted = percentCompleted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public Project(){
        name = client = "";
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String[] getDescription() {
        return description;
    }

    public void setDescription(String[] description) {
        this.description = description;
    }

    public String[] getStartDate() {
        return startDate;
    }

    public void setStartDate(String[] startDate) {
        this.startDate = startDate;
    }

    public String[] getEndDate() {
        return endDate;
    }

    public void setEndDate(String[] endDate) {
        this.endDate = endDate;
    }

    public String[] getStatus() {
        return status;
    }

    public void setStatus(String[] status) {
        this.status = status;
    }

    public String[] getPercentage() {
        return percentage;
    }

    public void setPercentage(String[] percentage) {
        this.percentage = percentage;
    }
    
    
}
