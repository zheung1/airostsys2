/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

public class Member {
    private int id;
    private String name, course, faculty, phone, email, position, username, password;
    private String[] skillName= {"","","","","","","","","",""};
    private String[] skillLevel = {"","","","","","","","","",""};
    private String[] achievement = {"","","","","","","","","",""};
    private String[] achievementYear = {"","","","","","","","","",""};
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String[] getSkillName() {
        return skillName;
    }

    public void setSkillName(String[] skillName) {
        this.skillName = skillName;
    }

    public String[] getSkillLevel() {
        return skillLevel;
    }

    public void setSkillLevel(String[] skillLevel) {
        this.skillLevel = skillLevel;
    }

    public String[] getAchievement() {
        return achievement;
    }

    public void setAchievement(String[] achievement) {
        this.achievement = achievement;
    }

    public String[] getAchievementYear() {
        return achievementYear;
    }

    public void setAchievementYear(String[] achievementYear) {
        this.achievementYear = achievementYear;
    }

    
}
