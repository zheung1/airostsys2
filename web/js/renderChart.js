/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
google.charts.load('current', {'packages':['gantt']});
google.charts.setOnLoadCallback(drawChart);

function drawChart(phases, startDates, endDates) {
     
    phases = phases.substring(1, phases.length - 1);
    startDates = (startDates.substring(1, startDates.length - 1)).replace(/\s/g,'');
    endDates = (endDates.substring(1, endDates.length - 1)).replace(/\s/g,'');
   
    var phaseArray = [];
    var sDateArray = [];
    var eDateArray = [];
    phaseArray = phases.split(',');
    sDateArray = startDates.split(',');
    eDateArray = endDates.split(',');
    
    var startDayArr = [], startMonthArr = [], startYearArr = [];
    var endDayArr = [], endMonthArr = [], endYearArr = [];
    
      for(var i=0; i<10; i++){
          if(sDateArray[i] != null){
            var temp = [];
            var temp2 = [];
            
            temp = sDateArray[i].split('-');
            startYearArr[i] =  temp[0];
            startMonthArr[i] =  temp[1];
            startDayArr[i] =  temp[2];
            
            
            temp2 = eDateArray[i].split('-');
            endYearArr[i] =  temp2[0];
            endMonthArr[i] =  temp2[1];
            endDayArr[i] =  temp2[2];
          
        }else{
              continue;
          }
      }
      
      var data = new google.visualization.DataTable();
      
      data.addColumn('string', 'Phase ID');
      data.addColumn('string', 'Phase Name');
      data.addColumn('string', '');
      data.addColumn('date', 'Start Date');
      data.addColumn('date', 'End Date');
      data.addColumn('number', '');
      data.addColumn('number', '');
      data.addColumn('string', '');


    for(var i=0; i<10; i++){
        
        if(phaseArray[i]!="  ")
            data.addRow([(i+1).toString(), phaseArray[i].substring(1,phaseArray[i].length), 'spring',
            new Date(parseInt(startYearArr[i]), parseInt(startMonthArr[i]), parseInt(startDayArr[i])), new Date(parseInt(endYearArr[i]), parseInt(endMonthArr[i]), parseInt(endDayArr[i])), null, 100, null]);
        else
            break;
    }
    
      var options = {
        height: 400,
        gantt: {
          trackHeight: 30
        }
      };

      var chart = new google.visualization.Gantt(document.getElementById('chart_div'));
      
      var container = document.getElementById('chart_div');
  var chart = new google.visualization.Gantt(container);

  google.visualization.events.addOneTimeListener(chart, 'ready', function () {
    var observer = new MutationObserver(function (nodes) {
      Array.prototype.forEach.call(nodes, function(node) {
        if (node.addedNodes.length > 0) {
          Array.prototype.forEach.call(node.addedNodes, function(addedNode) {
            if ((addedNode.tagName === 'rect') && (addedNode.getAttribute('fill') === 'white')) {
              addedNode.setAttribute('fill', 'transparent');
              addedNode.setAttribute('stroke', 'transparent');
              Array.prototype.forEach.call(addedNode.parentNode.getElementsByTagName('text'), function(label) {
                label.setAttribute('fill', 'transparent');
              });
            }
          });
        }
      });
    });
    observer.observe(container, {
      childList: true,
      subtree: true
    });
  });
      
      chart.draw(data, options);
}


