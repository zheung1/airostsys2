<%@page import="Model.User"%>
<%@page import="java.util.Arrays"%>
<%@page import="Model.Member"%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/w3.css">
  <link rel="stylesheet" href="css/sidebar.css" />
  <link rel="stylesheet" href="css/popup.css" />
  <%
        Member member = (Member)session.getAttribute("member");
  %>
 
</head>
<body>
<!--nav bar-->
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.jsp">HOME</a>
            </li>
            
        </ul>
    </div>
    <div class="mx-auto order-0">
        <!--<a class="navbar-brand mx-auto" href="#">Navbar 2</a>-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            
            <li class="nav-item">
                <%
                    User user = (User)session.getAttribute("user");
                    if(user==null){
                %>    
                <a class="nav-link" href="#popup1">Login</a>
            </li>
                <%}else{
                    %>
                    <a class="nav-link" href="logoutController">Logout</a>
             </li>
                <%}%>
            
        </ul>
    </div>
</nav>

    <!-- The sidebar -->
<div class="sidebar">
  <a class="active" href="#home">Inventory Manager</a>
  <a href="projectController">Project Manager</a>
  <a href="blogEditor.jsp">New Blog Post</a>
  <a href="attendanceController">Record attendance</a>
  <%if(user.getPosition().equals("ajk")){%>
  <a href="teamController">Team Manager</a>
  <a href="trainerController">Train attendance system</a>
  <a href="attendanceViewerController">Attendance record viewer</a>
  <%}%>
</div>
    
    
<div class="content">
    <h3>Member information </h3>
    <p>Click "save" to save changes</p>
    
        <form  action="saveMemberEditController" method="post" >
                        <input type="hidden" name="id" value="<%=member.getId()%>"/>
                        <label>Name</label><br/>
                        <input type="text" class="w3-input w3-round w3-border" value="<%=member.getName()%>" name="name" placeholder="member name" required></input><br/>
                        <label>Course</label><br/>
                        <input type="text" class="w3-input w3-round w3-border" value="<%=member.getCourse()%>" name="course" placeholder="course" required></input><br/>
                        <label>Faculty</label><br/>
                        <input type="text" class="w3-input w3-round w3-border" value="<%=member.getFaculty()%>" name="faculty" placeholder="faculty" required></input><br/>
                        <label>Phone number</label><br/>
                        <input type="text" class="w3-input w3-round w3-border" value="<%=member.getPhone()%>" name="phone" placeholder="phone number" required></input><br/>
                        <label>Email</label><br/>
                        <input type="email" class="w3-input w3-round w3-border" value="<%=member.getEmail()%>" name="email" placeholder="email" required></input><br/>
                        <label>Position</label><br/>
                        <!--input type="text" class="w3-input w3-round w3-border" value="<%--=member.getPosition()--%>" name="position" placeholder="member position" required></input><br/-->
                        <select name="position" class="w3-input w3-round w3-border" required>
                            <option value="ajk">Team AJK</option>
                            <option value="member">Team member</option>
                        </select>
                        <h3>Skills</h3>
                        <p>You can fill in up to 10 important skills of the member</p>
                        <table class="table">
                            <tr>
                            <th>Skill name</th>
                            <th>Skill level</th>
                            </tr>
                            <%for(int i=0; i<10; i++){%>
                            <tr>
                                <td>
                                    <input type="text" name="skill<%=i%>" value="<%=member.getSkillName()[i].substring(1,member.getSkillName()[i].length())%>" placeholder="maximum 40 chars" maxlength="40" />
                                </td>
                                <td>
                                    <select name="skillLevel<%=i%>">
                                        <option value="5">5</option>
                                        <option value="4">4</option>
                                        <option value="3">3</option>
                                        <option value="2">2</option>
                                        <option value="1">1</option>
                                        
                                    </select>
                                </td>
                            </tr>
                            <%}%>
                        </table>
                        
                        <h3>Achievements</h3>
                        <p>You can fill in up to 10 notable achievements of the member</p>
                        <table class="table">
                            <tr>
                            <th>Achievement name</th>
                            <th>Achievement year</th>
                            </tr>
                            <%for(int i=0; i<10; i++){%>
                            <tr>
                                <td>
                                    <input type="text" name="achievement<%=i%>" value="<%=member.getAchievement()[i].substring(1,member.getAchievement()[i].length())%>" placeholder="maximum 40 chars" maxlength="40" />
                                </td>
                                <td>
                                    <input type="number" name="achievementYear<%=i%>" value="<%=member.getAchievementYear()[i]%>" placeholder="year" maxlength="4" />
                                </td>
                            </tr>
                            <%}%>
                        </table>
                        
                        <input type="submit" value="Save"/>
          </form>
</div>

</body>
</html>


