<%@page import="java.util.Arrays"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Attendance scanner</title>
  <script defer src="js/face-api.min.js"></script>
  <script defer src="js/attendance.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
  <script>
      <%
          String members = (String)session.getAttribute("memList");
      %>
function loadLabeledImages() {
  const labels = <%=members%>;
  return Promise.all(
    labels.map(async label => {
      const descriptions = []
      
      for (let i = 1; i <= 2; i++) {
        const img = await faceapi.fetchImage(`labeled_images/`+label+`/`+i+`.jpg`);
        const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor();
        //descriptions.push(detections.descriptor);
		
      }
		
      return new faceapi.LabeledFaceDescriptors(label, descriptions)
    })
  )
}
  </script>
  <style>
   /*body {
      margin: 0;
      padding: 0;
      width: 100vw;
      height: 100vh;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    canvas {
      position: absolute;
    }*/
	
	
	.layer1 {
    position:absolute;
    z-index: 1;
}

#frame canvas {
    position:absolute;
    z-index: 2;
}

#frame{
	position: relative;
}

#list{
	height: 70%;
	overflow: auto;
}
  </style>

</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <!-- Brand/logo -->
  <a class="navbar-brand" href="#">Airost</a>
  
  
</nav>


<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <div id="frame">
		<video id="video" class="layer1" width="500" height="500" autoplay muted></video>
	  </div>
    </div>
    <div class="col-sm-6">
		<br>
		<h3>Step 1</h3>
		<h4>Click the button below after the camera highlighted your face.</h4>
		<button class="btn btn-primary" onclick="takeSnapshot()">Take Snapshot</button><br><br>
		
		<h3>Step 2</h3>
		<h4>Click the button below to record your attendance</h4>
		<button class="btn btn-primary" id="img2" style="z-index: 1000">Record Attendance</button>
		
	</div>
    
    
  </div>
  
  <div class="row">
    <div class="col-sm-6">
      
    </div>
    <div class="col-sm-6" style="height: 40%;">
      <br>
	  <div id="list" style="height: 150px;">
	  
		<table id="atable" class="table">
			<tr>
				<th>Name</th>
				<th>Time</th>
				<th>Date</th>
			</tr>
		</table>
	  </div>
    </div>
    
  </div>
</div>

 <div id="results"></div> 
  
  
  
  
  
  
  
  
  <!--hidden stuff-->
  <img id="imgz" src="" style="height: 1px; overflow: hidden; width: 30%; position: absolute; visibility: hidden;"/>
  <canvas style="height: 1px; overflow: hidden; position: absolute; visibility: hidden;" id="snap"></canvas>
  <div id="imgView" style="height: 1px; overflow: hidden; visibility: hidden;" >hello</div>
  </body>
</html>
