<%@page import="Model.User"%>
<%@page import="Model.Blog"%>
<%@page import="Model.Member"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/w3.css">
  <link rel="stylesheet" href="css/sidebar.css" />
  <link rel="stylesheet" href="css/coverart.css" />
  <link rel="stylesheet" href="css/popup.css" />
  <style>

</style>
  
</head>
<body>
    
<!--nav bar-->
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <%
                User user = (User)session.getAttribute("user");
                //if(user!=null){%>
            <li class="nav-item active">
                <a class="nav-link" href="index.jsp">Home</a>
            </li>
            <%//}%>
            
        </ul>
    </div>
    <div class="mx-auto order-0">
        <!--<a class="navbar-brand mx-auto" href="#">Navbar 2</a>-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            
            <li class="nav-item">
                <%
                    
                    if(user==null){
                %>    
                <a class="nav-link" href="#popup1">Login</a>
            </li>
                <%}else{
                    %>
                    <a class="nav-link" href="logoutController">Logout</a>
             </li>
                <%}%>
            
        </ul>
    </div>
</nav>

<div id="popup1" class="overlay" style="z-index: 10;">
	<div class="popup" style="height: auto; z-index:10;">
		<h2>Login</h2>
		<a class="close" href="#">&times;</a>
		<div class="contentPopup">
                    <form method="post" action="loginController">
                        <label>Username</label><br>
                        <input type="text" class="w3-input w3-round w3-border" name="name"/><br>
                        <label>Password</label><br>
                        <input type="password" class="w3-input w3-round w3-border" name="password"/><br>
                        <input type="submit" class="w3-indigo w3-round" value="Log In"/>
                    </form>
		</div>
	</div>
</div> 
<div class="hero-image" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('css/thelost.jpg')">
  <div class="hero-text">
    <h1 style="font-size:50px">NEWSROOM</h1>
    <p>Click on one the articles to see more</p>
    <%
        ArrayList<Blog> blogs = (ArrayList)session.getAttribute("blogList");
    %>
    
  </div>
    
</div>
        
    <div class="container" >
        <h2>News</h2>
        <div class="card-group"> 
            <div class="row"> 
 <%for(int i=0; i<blogs.size(); i++){%>
 <div class="col-sm-4"> 
 <div class="flip-card">
  <div class="flip-card-inner">
    <div class="flip-card-front">
      <img src="images/<%=blogs.get(i).getImage()%>" alt="Avatar" style="width:300px;height:300px;">
    </div>
    <div class="flip-card-back">
      <h1><%=blogs.get(i).getName()%></h1> 
      <p>by <%=blogs.get(i).getAuthor()%> <%=blogs.get(i).getDate()%></p> 
      <p><%=blogs.get(i).getDescription()%></p>
      <button class="w3-button w3-indigo w3-round" onclick="displayBlog('<%=blogs.get(i).getName()%>'); window.location.href='#popup6'">Show more</button>
      
      <!--form action="showBlogController" method="post">
          <input type="hidden" value="<%--=blogs.get(i).getId()--%>" name="id"/>
          <input type="submit"/>
      </form-->
    </div>
  </div>
</div>
 </div>
 <%}%>
            </div> 
        </div> 
    </div> 

 <script>
    function displayBlog(title){
        $('#blog').load("blogs/"+title+".html");
    }
</script>
<div id="popup6" class="overlay" style="z-index:10;">
	<div class="popup" style="z-index:10;">
		<a class="close" href="#">&times;</a>
		<div class="contentPopup" id="blog">
                    
		</div>
	</div>
</div>
</body>
</html>

