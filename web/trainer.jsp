<%@page import="Model.User"%>
<%@page import="Model.Item"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/w3.css">
  <link rel="stylesheet" href="css/sidebar.css" />
  <link rel="stylesheet" href="css/popup.css" />
  
  <style>

</style>
</head>
<body>
<!--nav bar-->
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.jsp">HOME</a>
            </li>
            
        </ul>
    </div>
    <div class="mx-auto order-0">
        <!--<a class="navbar-brand mx-auto" href="#">Navbar 2</a>-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            
            <li class="nav-item">
                <%
                    User user = (User)session.getAttribute("user");
                    if(user==null){
                %>    
                <a class="nav-link" href="#popup1">Login</a>
            </li>
                <%}else{
                    %>
                    <a class="nav-link" href="logoutController">Logout</a>
             </li>
                <%}%>
            
        </ul>
    </div>
</nav>

    <!-- The sidebar -->
<div class="sidebar">
  <a class="active" href="#home">Inventory Manager</a>
  <a href="projectController">Project Manager</a>
  <a href="blogEditor.jsp">New Blog Post</a>
  <a href="attendanceController">Record attendance</a>
  <%if(user.getPosition().equals("ajk")){%>
  <a href="teamController">Team Manager</a>
  <a href="trainerController">Train attendance system</a>
  <a href="attendanceViewerController">Attendance record viewer</a>
  <%}%>
</div>
<%
    String[]members = (String[])session.getAttribute("mList");
%>
<div class="content">
    <h3>Attendance system trainer</h3>
    <p>The attendance system will only work after it is trained with photos of all users</p>
    <form action="saveTrainController" method="post" enctype="multipart/form-data">
    <label>Username</label>
    <select name="name">
        <%for(int i=0; i<members.length; i++){%>
        <option value="<%=members[i]%>"><%=members[i]%></option>
        <%}%>
    </select>
    <br>
    <label>Please select 5 jpg with numbered names (1.jpg, 2.jpg, n.jpg)</label><br>
    <input name="file" type="file" id="file" multiple>
    <br>
    <input type="submit" value="save"/>
    </form>
</div>
    


</body>
</html>

