
<%@page import="Model.User"%>
<%@page import="Model.Project"%>
<%@page import="Model.Item"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/w3.css">
  <link rel="stylesheet" href="css/sidebar.css" />
  <link rel="stylesheet" href="css/popup.css" />
  <style>

</style>
</head>
<body>
<!--nav bar-->
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.jsp">HOME</a>
            </li>
            
        </ul>
    </div>
    <div class="mx-auto order-0">
        <!--<a class="navbar-brand mx-auto" href="#">Navbar 2</a>-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            
            <li class="nav-item">
                <%
                    User user = (User)session.getAttribute("user");
                    if(user==null){
                %>    
                <a class="nav-link" href="#popup1">Login</a>
            </li>
                <%}else{
                    %>
                    <a class="nav-link" href="logoutController">Logout</a>
             </li>
                <%}%>
            
        </ul>
    </div>
</nav>
    <!-- The sidebar -->
<div class="sidebar">
  <a class="active" href="#home">Inventory Manager</a>
  <a href="projectController">Project Manager</a>
  <a href="blogEditor.jsp">New Blog Post</a>
  <a href="attendanceController">Record attendance</a>
  <%if(user.getPosition().equals("ajk")){%>
  <a href="teamController">Team Manager</a>
  <a href="trainerController">Train attendance system</a>
  <a href="attendanceViewerController">Attendance record viewer</a>
  <%}%>
</div>
<div class="content">
    <button onclick="window.location.href='#popup1'" type="button"  class="w3-button w3-round w3-indigo w3-border w3-hover-green">Add Project</button>
<table class="table">
    <tr>
        <th>No</th>
        <th>Project Name</th>
        <th>Client</th>
        <th>Progress</th>
        <th>Percentage completed</th>
        <th>Action</th>
        
    </tr>
    <%
        ArrayList<Project>projects = (ArrayList)session.getAttribute("projectList");
        for(int i=0; i<projects.size(); i++){
    %>
    <tr>
        <td>
            <%=(i+1)%>
        </td>
        <td>
            <%=projects.get(i).getName()%>
        </td>
        <td>
            <%=projects.get(i).getClient()%>
        </td>
        <td>
            <%=projects.get(i).getProgress()%>
        </td>
        <td>
            <%=projects.get(i).getPercentCompleted()%>
        </td>
        <td>
            <form> 
                <input  type="hidden" style="visibility: hidden; position:absolute;" name="id" value="<%=projects.get(i).getId()%>"/>
                <input formaction="editProjectController" type="submit" class="w3-button w3-indigo w3-round" value="update"/>
                <input formaction="removeProjectController" type="submit" class="w3-button w3-red w3-round" value="remove"/>
            </form>
        </td>
    </tr>
    <%}%>
</table>
</div>
    
<div id="popup1" class="overlay">
	<div class="popup" style="width: 80%;">
		<h2>Add new project</h2>
		<a class="close" href="#">&times;</a>
		<div class="contentPopup" >
                    <form  action="saveProjectController" method="post">
                        <label>Name</label><br/>
                        <input type="text" class="w3-input w3-round w3-border" name="name" placeholder="project name" required></input><br/>
                        <label>Client</label><br/>
                        <input type="text" class="w3-input w3-round w3-border" name="client" placeholder="project client" required></input><br/>
                        <table class="table">
                            <tr>
                                <th>Phase no.</th>
                                <th>Phase name</th>
                                <th>Weightage</th>
                                <th>Status</th>
                                <th>Start date</th>
                                <th>End date</th>
                            </tr>
                           
                            <%
                            for(int i=0; i<10; i++){
                            %>
                            <tr>
                                <td><%=i%></td>
                                <td>
                                    <input type="text" name="phaseName<%=i%>" maxlength="50" placeholder="max 50 chars"/>
                                </td>
                                <td>
                                    <input type="number" name="phaseWeightage<%=i%>" maxlength="3" value="0" />
                                </td>
                                <td>
                                    <input type="radio" id="completed<%=i%>" name="phaseStatus<%=i%>" value="completed">Completed<br/>
                                    <input type="radio" id="ongoing<%=i%>" name="phaseStatus<%=i%>" value="ongoing">Ongoing
                                </td>
                                <td>
                                    <input type="date" name="phaseSdate<%=i%>"/>
                                </td>
                                <td>
                                    <input type="date" name="phaseEdate<%=i%>"/>
                                </td>
                            </tr>
                            <%
                            }
                            %>
                           
                        </table>
                        <input type="submit" value="Save project" class="w3-button w3-round w3-indigo w3-border w3-hover-green"> </input>
                    </form>
		</div>
	</div>
</div>

                         
                            
</body>
</html>

