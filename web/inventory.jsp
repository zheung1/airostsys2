<%@page import="Model.User"%>
<%@page import="Model.Item"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/w3.css">
  <link rel="stylesheet" href="css/sidebar.css" />
  <link rel="stylesheet" href="css/popup.css" />
  
  <style>

</style>
</head>
<body>
<!--nav bar-->
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.jsp">HOME</a>
            </li>
            
        </ul>
    </div>
    <div class="mx-auto order-0">
        <!--<a class="navbar-brand mx-auto" href="#">Navbar 2</a>-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            
            <li class="nav-item">
                <%
                    User user = (User)session.getAttribute("user");
                    if(user==null){
                %>    
                <a class="nav-link" href="#popup1">Login</a>
            </li>
                <%}else{
                    %>
                    <a class="nav-link" href="logoutController">Logout</a>
             </li>
                <%}%>
            
        </ul>
    </div>
</nav>

    <!-- The sidebar -->
<div class="sidebar">
  <a class="active" href="#home">Inventory Manager</a>
  <a href="projectController">Project Manager</a>
  <a href="blogEditor.jsp">New Blog Post</a>
  <a href="attendanceController">Record attendance</a>
  <%if(user.getPosition().equals("ajk")){%>
  <a href="teamController">Team Manager</a>
  <a href="trainerController">Train attendance system</a>
  <a href="attendanceViewerController">Attendance record viewer</a>
  <%}%>
</div>
<div class="content">
    <button onclick="window.location.href='#popup1'" type="button"  class="w3-button w3-round w3-indigo w3-border w3-hover-green">Add Item</button>
<table class="table">
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>Amount</th>
        <th>Location</th>
        <th>Action</th>
    </tr>
    <%
        ArrayList<Item>items = (ArrayList)session.getAttribute("itemList");
        for(int i=0; i<items.size(); i++){
    %>
        <tr>
            <td><%=(i+1)%></td>  
                    <th scope="row" class="">
                        <div class="p-1">
                            <img src="<%="images/"+items.get(i).getImage()%>" alt="" width="70" class="img-fluid rounded shadow-sm">
                          <div class="ml-1 d-inline-block align-middle">
                            <h5 class="mb-0 iname"><%=items.get(i).getName()%></h5>
                            <span class="text-muted font-weight-normal font-italic d-block"><%=items.get(i).getCategory()%></span>
                          </div>
                        </div>
                      </th>
                      <%if(items.get(i).getAmount()>0){%>
                      <td class="align-middle"><%=items.get(i).getAmount()%></td>
                      <%}else{%>
                        <td style="font-weight: bold; color: red;" class=" align-middle">Out Of Stock</td>
                      <%}%>
                      <td class=" align-middle"><%=items.get(i).getLocation()%></td>
                      
                      <td class=" align-middle">
                          <form > 
                           <input  type="hidden" style="visibility: hidden; position:absolute;" name="id" value="<%=items.get(i).getId()%>"/>
                           <input formaction="editItemController" type="submit" class="w3-button w3-indigo w3-round" value="edit"/>
                           <input formaction="removeItemController" type="submit" class="w3-button w3-red w3-round" value="remove"/>
                       </form>
                          
                      </td>
                      
    
        </tr>
    <%}%>
</table>
</div>
    
<div id="popup1" class="overlay">
	<div class="popup">
		<h2>Add new item</h2>
		<a class="close" href="#">&times;</a>
		<div class="contentPopup">
                    <form  action="saveItemController" method="post" enctype="multipart/form-data">
                        <label>Name</label><br/>
                        <input type="text" class="w3-input w3-round w3-border" name="name" placeholder="item name" required></input><br/>
                        <label>Category</label><br/>
                        <select name="category" class="w3-input w3-border w3-round" required>
                            <option value="electronics">Electronic Device</option>
                            <option value="tools">Tools</option>
                            <option value="materials">Materials</option>
                            <option value="stationery">Stationery</option>
                        </select>
                        <label>Location</label><br/>
                        <input type="text" class="w3-input w3-round w3-border" name="location" placeholder="location" required></input><br/>
                        <label>Amount</label><br/>
                        <input type="text" class="w3-input w3-round w3-border" name="amount" placeholder="amount" required></input><br/>
                        <label>Description</label><br/>
                        <textarea name="description" class="w3-input w3-round w3-border" placeholder="brief description" rows="10" required></textarea><br>
                        <label>Image</label><br/>
                        <input class="w3-input w3-border w3-round" type="file" name="file" multiple="true" required/><br>
                        <input type="submit" value="Save item" class="w3-button w3-round w3-indigo w3-border w3-hover-green"> </input>
                    </form>
		</div>
	</div>
</div>

</body>
</html>

