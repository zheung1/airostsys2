<%@page import="Model.User"%>
<%@page import="Model.Item"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/qrcode.js"></script>
  <link rel="stylesheet" href="css/w3.css">
  <link rel="stylesheet" href="css/sidebar.css" />
  <link rel="stylesheet" href="css/popup.css" />
  <style>

</style>
</head>
<body>
<!--nav bar-->
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.jsp">HOME</a>
            </li>
            
        </ul>
    </div>
    <div class="mx-auto order-0">
        <!--<a class="navbar-brand mx-auto" href="#">Navbar 2</a>-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            
            <li class="nav-item">
                <%
                    User user = (User)session.getAttribute("user");
                    if(user==null){
                %>    
                <a class="nav-link" href="#popup1">Login</a>
            </li>
                <%}else{
                    %>
                    <a class="nav-link" href="logoutController">Logout</a>
             </li>
                <%}%>
            
        </ul>
    </div>
</nav>

    <!-- The sidebar -->
<div class="sidebar">
  <a class="active" href="#home">Inventory Manager</a>
  <a href="projectController">Project Manager</a>
  <a href="blogEditor.jsp">New Blog Post</a>
  <a href="attendanceController">Record attendance</a>
  <%if(user.getPosition().equals("ajk")){%>
  <a href="teamController">Team Manager</a>
  <a href="trainerController">Train attendance system</a>
  <a href="attendanceViewerController">Attendance record viewer</a>
  <%}%>
</div>
<div class="content">
    <h3>Item information </h3>
    <p>Click "save" to save changes <button onclick="window.location.href='#popup1'" class="w3-button w3-round w3-green">Generate QR</button></p>
    
    <%
        Item item = (Item)session.getAttribute("item");
        
    %>
    <label>Image</label><br>
    <img style="width: 10%;" src="<%="images/"+item.getImage()%>"/>
        <form  action="saveEditController" method="post">
                        <input type="hidden" value="<%=item.getId()%>" name="id"/>
                        <label>Name</label><br/>
                        <input type="text" value="<%=item.getName()%>" class="w3-input w3-round w3-border" name="name" placeholder="item name" required></input><br/>
                        <label>Category</label><br/>
                        <select name="category" class="w3-input w3-border w3-round" required>
                            <option value="electronics">Electronic Device</option>
                            <option value="tools">Tools</option>
                            <option value="materials">Materials</option>
                            <option value="stationery">Stationery</option>
                        </select>
                        <label>Location</label><br/>
                        <input type="text" value="<%=item.getLocation()%>" class="w3-input w3-round w3-border" name="location" placeholder="location" required></input><br/>
                        <label>Amount</label><br/>
                        <input type="text" value="<%=item.getAmount()%>" class="w3-input w3-round w3-border" name="amount" placeholder="amount" required></input><br/>
                        <label>Description</label><br/>
                        <textarea name="description"  class="w3-input w3-round w3-border" placeholder="brief description" rows="10" required><%=item.getDescription()%></textarea><br>
                        <input type="submit" value="Save item" class="w3-button w3-round w3-indigo w3-border w3-hover-green"> </input>
    </form>
</div>
    <div id="popup1" class="overlay">
	<div class="popup">
		
		<a class="close" href="#">&times;</a>
		<div class="contentPopup">
                    <div id="qrcode"></div>
                    <button class="w3-button w3-round w3-indigo w3-border w3-hover-green" onclick="printElem('qrcode')">
                        Print
                    </button>
                    <script type="text/javascript">
                        new QRCode(document.getElementById("qrcode"), "http://localhost:8080/AirostSys2/editItemController?id="+<%=item.getId()%>);
                        
  function printElem(divId) {
    var content = document.getElementById(divId).innerHTML;
    var mywindow = window.open('', 'Print', 'height=600,width=800');

    mywindow.document.write('<html><head><title>Print</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write(content);
    mywindow.document.write('</body></html>');

    mywindow.document.close();
    mywindow.focus()
    mywindow.print();
    mywindow.close();
    return true;
}
                    </script>
		</div>
	</div>
</div>
</body>
</html>


