<%@page import="Model.User"%>
<%@page import="java.util.Arrays"%>
<%@page import="Model.Project"%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/loader.js"></script>
  <script src="js/renderChart.js"></script>
  <script src="js/qrcode.js"></script>
  
  <link rel="stylesheet" href="css/w3.css">
  <link rel="stylesheet" href="css/sidebar.css" />
  <link rel="stylesheet" href="css/popup.css" />
  <%
        Project project = (Project)session.getAttribute("project");
  %>
 
</head>
<body>
<!--nav bar-->
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.jsp">HOME</a>
            </li>
            
        </ul>
    </div>
    <div class="mx-auto order-0">
        <!--<a class="navbar-brand mx-auto" href="#">Navbar 2</a>-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            
            <li class="nav-item">
                <%
                    User user = (User)session.getAttribute("user");
                    if(user==null){
                %>    
                <a class="nav-link" href="#popup1">Login</a>
            </li>
                <%}else{
                    %>
                    <a class="nav-link" href="logoutController">Logout</a>
             </li>
                <%}%>
            
        </ul>
    </div>
</nav>

    <!-- The sidebar -->
<div class="sidebar">
  <a class="active" href="#home">Inventory Manager</a>
  <a href="projectController">Project Manager</a>
  <a href="blogEditor.jsp">New Blog Post</a>
  <a href="attendanceController">Record attendance</a>
  <%if(user.getPosition().equals("ajk")){%>
  <a href="teamController">Team Manager</a>
  <a href="trainerController">Train attendance system</a>
  <a href="attendanceViewerController">Attendance record viewer</a>
  <%}%>
</div>
    
    
<div class="content">
    <h3>Project information </h3>
    <p>Click "save" to save changes <button onclick="drawChart('<%=Arrays.toString(project.getDescription())%>','<%=Arrays.toString(project.getStartDate())%>','<%=Arrays.toString(project.getEndDate())%>'); window.location.href='#popup3'; "  class="w3-button w3-round w3-green">View Gantt Chart</button></p>
    <!--drawChart('<%--=Arrays.toString(project.getDescription())%>','<%=Arrays.toString(project.getStartDate())%>','<%=Arrays.toString(project.getEndDate())--%>');-->
    
        <form  action="saveProjectChangesController" method="post">
                        <input type="hidden" name="id" value="<%=project.getId()%>"/>
                        <label>Name</label><br/>
                        <input type="text" class="w3-input w3-round w3-border" value="<%=project.getName()%>" name="name" placeholder="project name" required></input><br/>
                        <label>Client</label><br/>
                        <input type="text" class="w3-input w3-round w3-border" value="<%=project.getClient()%>" name="client" placeholder="project client" required></input><br/>
                        <table class="table">
                            <tr>
                                <th>Phase no.</th>
                                <th>Phase name</th>
                                <th>Weightage</th>
                                <th>Status</th>
                                <th>Start date</th>
                                <th>End date</th>
                            </tr>
                           
                            <%
                            for(int i=0; i<10; i++){
                            %>
                            <tr>
                                <td><%=i%></td>
                                <td>
                                    <input type="text" value="<%=project.getDescription()[i].substring(1,project.getDescription()[i].length())%>" name="phaseName<%=i%>" maxlength="50" placeholder="max 50 chars"/>
                                </td>
                                <td>
                                    
                                    <input type="number" value="<%=project.getPercentage()[i].replaceAll("\\s","")%>" name="phaseWeightage<%=i%>" maxlength="3"  />
                                </td>
                                <td>
                                    <%if(project.getStatus()[i].replaceAll("\\s","").equals("completed")){%>
                                    <input type="radio" id="completed<%=i%>" name="phaseStatus<%=i%>" value="completed" checked>Completed<br/>
                                    <input type="radio" id="ongoing<%=i%>" name="phaseStatus<%=i%>" value="ongoing">Ongoing
                                    <%}else if(project.getStatus()[i].replaceAll("\\s","").equals("ongoing")){%>
                                    <input type="radio" id="completed<%=i%>" name="phaseStatus<%=i%>" value="completed" >Completed<br/>
                                    <input type="radio" id="ongoing<%=i%>" name="phaseStatus<%=i%>" value="ongoing" checked>Ongoing
                                    <%}else{%>
                                    <input type="radio" id="completed<%=i%>" name="phaseStatus<%=i%>" value="completed" >Completed<br/>
                                    <input type="radio" id="ongoing<%=i%>" name="phaseStatus<%=i%>" value="ongoing">Ongoing
                                    
                                    <%}%>
                                </td>
                                <td>
                                    <input type="date" value="<%=project.getStartDate()[i].replaceAll("\\s","")%>" name="phaseSdate<%=i%>"/>
                                </td>
                                <td>
                                    <input type="date" value="<%=project.getEndDate()[i].replaceAll("\\s","")%>" name="phaseEdate<%=i%>"/>
                                </td>
                            </tr>
                            <%
                            }
                            %>
                           
                        </table>
                        <input type="submit" value="Save changes" class="w3-button w3-round w3-indigo w3-border w3-hover-green"> </input>
        </form>
</div>
 
                        
<div id="popup3" class="overlay">
	<div class="popup" style="width: 70%">
		<h2>Gantt Chart</h2>
		<a class="close" href="#">&times;</a>
		<div class="contentPopup">
                     
                    <div id="chart_div"></div>
		</div>
	</div>
</div>
</body>
</html>


