<%@page import="Model.User"%>
<%@page import="Model.Item"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="//cdn.jsdelivr.net/ckeditor/4.0.1/ckeditor.js"></script>
  <link rel="stylesheet" href="css/w3.css">
  <link rel="stylesheet" href="css/sidebar.css" />
  <link rel="stylesheet" href="css/popup.css" />
  
      
</head>
<body>
<!--nav bar-->
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.jsp">HOME</a>
            </li>
            
        </ul>
    </div>
    <div class="mx-auto order-0">
        <!--<a class="navbar-brand mx-auto" href="#">Navbar 2</a>-->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            
            <li class="nav-item">
                <%
                    User user = (User)session.getAttribute("user");
                    if(user==null){
                %>    
                <a class="nav-link" href="#popup1">Login</a>
            </li>
                <%}else{
                    %>
                    <a class="nav-link" href="logoutController">Logout</a>
             </li>
                <%}%>
            
        </ul>
    </div>
</nav>

    <!-- The sidebar -->
<div class="sidebar">
  <a class="active" href="#home">Inventory Manager</a>
  <a href="projectController">Project Manager</a>
  <a href="blogEditor.jsp">New Blog Post</a>
  <a href="attendanceController">Record attendance</a>
  <%if(user.getPosition().equals("ajk")){%>
  <a href="teamController">Team Manager</a>
  <a href="trainerController">Train attendance system</a>
  <a href="attendanceViewerController">Attendance record viewer</a>
  <%}%>
</div>
    
<div class="content">
    <h2>New blogpost</h2>
    <form method="post" action="blogController" enctype="multipart/form-data">
        <label>Title</label><br>
        <input type="text" name="title" placeholder="max 50 length" maxlength="50" class="w3-input w3-round w3-border"/><br>
        <label>Author</label><br>
        <input type="text" name="author" class="w3-input w3-round w3-border"/><br>
        <label>Date</label><br>
        <input type="date" name="date" class="w3-input w3-round w3-border"/><br>
        <label>Short Description</label><br>
        <input type="text" name="description" class="w3-input w3-round w3-border" maxlength="200" placeholder="max 200 chars"/><br>
        <label>Image</label><br/>
        <input class="w3-input w3-border w3-round" type="file" name="file" multiple="true" required/><br>    
        <textarea name="blogContent" id="editor1" rows="10" cols="80">
                
            </textarea>
            <script>
                window.onload = function(){
                CKEDITOR.replace('editor1');
            }
            </script>
        
            <input type="submit" value="Save"/>
    </form>
</div>
    


</body>
</html>

