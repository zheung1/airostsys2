window.onload = function(){
	 var container = document.getElementById("imgView");
	 container.innerHTML = "model is loading";
}

const video = document.getElementById('video')

Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri('./md'),
  faceapi.nets.faceRecognitionNet.loadFromUri('./md'),
  faceapi.nets.faceLandmark68Net.loadFromUri('./md'),
  faceapi.nets.faceExpressionNet.loadFromUri('./md'),
  faceapi.nets.ssdMobilenetv1.loadFromUri('./md'),
  faceapi.loadMtcnnModel('./md'),
  faceapi.loadFaceRecognitionModel('./md')
]).then(start)

async function start() {
  
  var container = document.getElementById("imgView");
  var list = document.getElementById("list");
  //container.style.position = 'relative'
  const labeledFaceDescriptors = await loadLabeledImages()
  const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6)
  let image
  let canvas
  container.append('Loaded')
  startVideo();
  
  document.getElementById("img2").addEventListener('click', async () => {
    //alert('clck');
	//if (image) image.remove()
    if (canvas) canvas.remove()
    //image = await faceapi.bufferToImage(imageUpload.files[0])
    //container.append(image)
    //image = await faceapi.bufferToImage(document.getElementById("imgz"));
	
	image = document.getElementById("imgz");
	container.append(image);
	canvas = faceapi.createCanvasFromMedia(image)
    container.append(canvas)
    const displaySize = { width: image.width, height: image.height }
    faceapi.matchDimensions(canvas, displaySize)
    const detections = await faceapi.detectAllFaces(image).withFaceLandmarks().withFaceDescriptors()
    const resizedDetections = faceapi.resizeResults(detections, displaySize)
    const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor))
    //alert('dx');
	if(results[0] == null){
		alert('No face detected. Please try again');
	}
	else{
	results.forEach((result, i) => {
      
	  const box = resizedDetections[i].detection.box
      const drawBox = new faceapi.draw.DrawBox(box, { label: result.toString() })
      
	  //list.append("<br>"+result.toString())
	//alert('d');
	var table = document.getElementById("atable");
	var row = document.createElement("tr");
	var name = document.createElement("td");
	var today = new Date();
	var d = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	var t = today.getHours() + ":" + today.getMinutes()+ ":" + today.getSeconds();
	var time = document.createElement("td");
	var date = document.createElement("td");
	var n = result.toString();
	//name.innerHTML = n.substring(0, n.length -6); 
	time.innerHTML = t;
	date.innerHTML = d;
	
	
		//alert('appenddone');
	var nama = result.toString();
        name.innerHTML = nama.substring(0, nama.length-3);
        nama = nama.substring(0, nama.length-3);
	var tarikh = d;
	var masa = t;
        row.appendChild(name);
	row.appendChild(time);
	row.appendChild(date);
	table.appendChild(row);
	alert(nama+"|"+tarikh+"|"+masa);
	
        $.post("storeAttendance", { nama: nama, tarikh:tarikh, masa:masa },
        function(data) {
	 $('#results').html(data);
	 alert('Your attendance is recorded successfully');
         
    });

	})
  }
  })
}

function startVideo() {
  navigator.getUserMedia = ( navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
  navigator.getUserMedia(
    { video: {} },
    stream => video.srcObject = stream,
    err => console.error(err)
  )
  var container = document.getElementById("imgView");
  container.append('Loaded')
}

video.addEventListener('play', () => {
  const canvas = faceapi.createCanvasFromMedia(video)
  //document.body.append(canvas)
  var vid = document.getElementById("frame");
  vid.appendChild(canvas)
  
  const displaySize = { width: video.width, height: video.height }
  faceapi.matchDimensions(canvas, displaySize)
  setInterval(async () => {
    const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions()
    const resizedDetections = faceapi.resizeResults(detections, displaySize)
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
    faceapi.draw.drawDetections(canvas, resizedDetections)
    faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
    faceapi.draw.drawFaceExpressions(canvas, resizedDetections)
  }, 100)
})


function takeSnapshot(){
       // alert('snap');
		// Here we're using a trick that involves a hidden canvas element.  

        var hidden_canvas = document.getElementById('snap'),
            context = hidden_canvas.getContext('2d');

		//var width = document.getElementById('video').width;
		//var height = document.getElementById('video').height;
		width = 1280;
		height = 720;
		
        if (width && height) {

            // Setup a canvas with the same dimensions as the video.
            hidden_canvas.width = width;
            hidden_canvas.height = height;

            // Make a copy of the current frame in the video on the canvas.
            context.drawImage(video, 0, 0, width, height);

            // Turn the canvas image into a dataURL that can be used as a src for our photo.
            var url = hidden_canvas.toDataURL('image/png');
			document.getElementById('imgz').src=url;
			//alert('snapshot done');
		}
}

